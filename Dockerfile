FROM python:3

WORKDIR /app
ENV PORT=80
EXPOSE 80

COPY ./requirements.txt /app/requirements.txt
RUN pip install -r requirements.txt

COPY . /app
ENTRYPOINT [ "python3", "main.py" ]
