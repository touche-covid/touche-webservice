from flask import Blueprint, jsonify, render_template
app = Blueprint('dashboard', __name__, template_folder="templates", static_folder="static")

#######################################################
# Provide routes

@app.route('/health-check', methods=['GET'])
def healthCheck():
    return jsonify({"success": True})

@app.route('/demo', methods=['GET'])
@app.route('/demo/<doorid>', methods=['GET'])
def demo(doorid=""):
    return render_template('demo.html', doorid=doorid)

@app.route('/', methods=['GET'])
def home():
    return render_template('home.html')
