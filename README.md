# Touché Covid WebService

This Web Service is [our](#Team) contribution to the Covid-19 crisis. It is part of a larger solution presented at a hackathon/startupweekend - April 2020.

This is also the home of the Dashboard.

## Installation and Deployment

### Getting ready

  1. First you need to clone the repository to your computer  
     `git clone git clone https://bitbucket.org/touche-covid/touche-webservice.git`  
     If you plan on making changes, you should work in your own branch:  
     `git checkout -b feature` (replace `feature` with a branch name describing the *feature* you are implementing or ticker number)
  2. Create a virtual environment with either one of the following two commands:
     * `python3 -m venv venv`
     * `virtualenv -p python3 venv`
  3. Load your virtualenvironement: `source venv/bin/activate`
  4. Install the dependencies: `pip install -r requirements.txt`

### Running

Just run it: `python main.py`

The server should start and be ready for connections at [`http://127.0.0.1:9000/`](http://127.0.0.1:9000/).

## Usage and Limitations

For health-check/up/ping purposes the following routes should always respond `{"success": true}` in JSON:

  - `/health-check`
  - `/api/health-check`
  - `/dashboard/health-check`

For testing purposes, you may visit the following routes in your WebBrowser:

  - `/api`  
    **WIP** should display OpenAPI Specification ;-)
  - `/dashboard`  
    The dashboard homepage.
  - `/dashboard/static/demo.html`  
    A demo of a static page. This is where CSS/JS should be.
  - `/dashboard/demo`  
    A demo of a template. (meant for this project developers).

## Global Architecture

![](README_architecture.png)

## Team

  - Andry
  - Anupam
  - Ialifinaritra
  - Nassim
  - Pierre
  - Voahary

## License

[MIT](https://choosealicense.com/licenses/mit/)
