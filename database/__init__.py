from flask_sqlalchemy import SQLAlchemy
import os, datetime

db = None
def initalize_db(app):
    global db
    if db is None:
        # by default we use an in-memory sqlite instance
        app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get('DATABASE_URL', 'sqlite://')
        app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
        db = SQLAlchemy(app)
    return db

def db_item2dict(row):
    result = {}
    for c in row.__table__.columns:
        value = getattr(row, c.name)
        if type(value) is datetime.datetime:
            value = value.replace(microsecond=0).isoformat() + 'Z'
        
        result[c.name] = value
    return result

def db_array_item2dict(rows):
    return [db_item2dict(row) for row in rows]