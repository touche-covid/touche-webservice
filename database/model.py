from . import db
import datetime

class Event(db.Model):
    uid = db.Column(db.Integer, primary_key=True, autoincrement=True)
    category = db.Column(db.String(80), nullable=False)
    confidence = db.Column(db.Float)
    device = db.Column(db.String(80))
    timestamp = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow)
