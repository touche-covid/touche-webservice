from flask import Blueprint, jsonify, request
from sqlalchemy import desc
from database import db, db_array_item2dict
from database.model import Event
from flask_socketio import emit
app = Blueprint('webservice', __name__)

from main import socketio

#######################################################
# Provide routes

@app.route('/health-check', methods=['GET'])
def healthCheck():
    return jsonify({"success": True})

@app.route('/events', methods=['POST'])
def newEvent():
    try:
        ### Log the event in the database
        data = request.get_json()
        assert(type(data) is dict)
        assert(type(data['category']) is str)
        if data['category'] not in ['test', 'face-nomask', 'face-mask', 'door-open']:
            return jsonify({"success": False, "error_message": "Invalid Category."}), 400
        
        if 'confidence' in data:
            assert(type(data['confidence']) in [int, float])
        if 'device' in data:
            assert(type(data['device']) is str)

        newEvent = Event(category=data['category'], confidence=data.get('confidence', None), device=data.get('device', None))
        db.session.add(newEvent)
        db.session.commit()
        
        ### Trigger the door if needed
        if 'device' in data and data['category'] == 'door-open':
            socketio.emit('opendoor', {'open':'true', 'doorid':data['device'] })
        
        return jsonify({"success": True}), 201
    except Exception as e:
        return jsonify({"success": False, "error_message": str(e)}), 400

@app.route('/events', methods=['GET'])
def allEvents():
    events = Event.query.order_by(desc(Event.timestamp)).limit(1000).all()
    return jsonify({"events": db_array_item2dict(events)})
    