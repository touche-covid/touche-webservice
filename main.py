from flask import Flask, render_template, jsonify
from flask_socketio import SocketIO, emit
import os, markdown, codecs
import time

app = Flask(__name__)
app.config['SECRET_KEY'] = os.environ.get('FLASK_SECRET', 'covid19')
socketio = SocketIO(app)
socketio.init_app(app, cors_allowed_origins="*")

############################################
# Load configuration

from dotenv import load_dotenv
try:
    load_dotenv()
except:
    pass # no .env file / ignore -- careful: might have been a bad format error

############################################
# Initialize the database

import database
database.initalize_db(app)

import database.model
database.db.create_all()

############################################
# Load routes

@app.route("/")
def home():
    input_file = codecs.open("README.md", mode="r", encoding="utf-8")
    text = input_file.read()
    input_file.close()

    return markdown.markdown(text)


@app.route('/health-check', methods=['GET'])
def healthCheck():
    return jsonify({"success": True})


from dashboard.routes import app as dashboard_routes
app.register_blueprint(dashboard_routes, url_prefix='/dashboard')

from webservice.routes import app as webservice_routes
app.register_blueprint(webservice_routes, url_prefix='/api')


############################################
# WebSocket routes for gate simulation

@app.route('/debug/open/<doorid>')
def debug_open(doorid):
    time.sleep(0.5)
    socketio.emit('opendoor', {'open':'true', 'doorid':doorid })
    return jsonify({"success": True})

@socketio.on('message')
def handle_message(message):
    print('received message: ' + message)

@socketio.on('my event')
def handle_my_custom_event(json):
    print('received json: ' + str(json))

# @socketio.on('connect')
# def connect():
#     # Wait for 5 seconds
#     time.sleep(5)
#     emit('opendoor', {'open':'true', 'doorid':1 })

@socketio.on('disconnect')
def disconnect():
    print('Client disconnected')

############################################

if __name__ == '__main__':
    socketio.run(
        app, host='0.0.0.0', port=int(os.environ.get('PORT', 8000)),
        debug=True, log_output=True)
